<?php

session_start();
include ('database_connection.php');

function stripPost($Post){
$Keys = array_keys($Post);
foreach($Keys as $Key){
$_SESSION[$Key] = $Post[$Key];
}
return;
}

function checkAnswer(){

return(intval($_SESSION["answer"])==intval($_SESSION["rightanswer"]));}

function getAnswers(){
$_SESSION["answers"] = array();
$answerquery = "select c_text,c_number from dbhomework.part_of natural join dbhomework.choice where q_number=".$_SESSION["currentquestion"]."";

$getanswers = mysql_query($answerquery) or die(mysql_error());
  while($row = mysql_fetch_assoc($getanswers)){ #If I use "array" instead of "assoc", it will print the results twice. I don't know why.
    $_SESSION["answers"][] = $row["c_text"];
	$_SESSION["maxc_number"] = intval($row["c_number"]);
	}
	
	return;
	}


function getNewRightAnswer(){
$rightanswerquery = "select c_number from dbhomework.part_of natural join dbhomework.choice where q_number=".$_SESSION["currentquestion"]." and correct=1";
$query_ex = mysql_query($rightanswerquery) or die(mysql_error());
$RightAnswerM = mysql_fetch_object($query_ex);
$RightAnswer = intval($RightAnswerM->c_number);
$_SESSION["rightanswer"] = intval($RightAnswer);
}

function printForm(){
echo '<head>';
    echo ' <link rel="stylesheet" href="quiz.css" type="text/css">';
echo '</head>';
echo "<div id=\"container\">";
echo $_SESSION["questions"][$_SESSION["currentquestion"]];

echo "<form action=\"\" method=\"post\">";
echo "<table>";
echo "<tr><td><input type=\"radio\" name=\"answer\" value=\"".intval($_SESSION["maxc_number"]-3)."\"></td><td>".$_SESSION["answers"][0]."</td></tr>";
echo "<tr><td><input type=\"radio\" name=\"answer\" value=\"".intval($_SESSION["maxc_number"]-2)."\"></td><td>".$_SESSION["answers"][1]."</td></tr>";
echo "<tr><td><input type=\"radio\" name=\"answer\" value=\"".intval($_SESSION["maxc_number"]-1)."\"></td><td>".$_SESSION["answers"][2]."</td></tr>";
echo "<tr><td><input type=\"radio\" name=\"answer\" value=\"".intval($_SESSION["maxc_number"])."\"></td><td>".$_SESSION["answers"][3]."</td></tr>";
echo "<tr><td> </td><td><input type=\"submit\" name=\"submit\"></td></tr></table>";
echo "</div>";


if($_SESSION["currentquestion"]==1){
$_SESSION["currentquestion"]++;}
if(isset($_POST["answer"])&& isset($_POST["submit"])){
$_SESSION["currentquestion"]++;
}


//I unset this manually so the indexes will remain correct.
unset($_SESSION["answers"]);
return;
}

function hallOfFame(){

$_SESSION["fame"] = array();
$query = "select best_score, name from dbhomework.users order by best_score desc limit 0,5";
$query_ex = mysql_query($query);
echo "<p><b>The hall of fame</b><br/></p>";
while($row = mysql_fetch_assoc($query_ex)){
echo "<b>{$row["name"]}</b> with <b>{$row["best_score"]}</b> points<br/>";
}
echo "<hr>";
}
?>