create table question (q_number int not null auto_increment,
		q_text varchar(250),
               primary key(q_number)) engine=InnoDB;

create table choice (c_number int not null auto_increment,
		c_text varchar(250),
		correct tinyint(1),
               primary key(c_number)) engine=InnoDB;

create table part_of(q_number int not null,
		 c_number int, 
		foreign key(q_number) references question(q_number), 
		foreign key(c_number) references choice(c_number), 
		primary key(q_number,c_number)) engine=InnoDB;
		
create table users (name varchar(50) not null,
		password varchar(25),
		email varchar(50),
               primary key(name)) engine=InnoDB;
			   
alter table user add column best_score int null;