<?php
  //Program by: Rogier krebbers
  //credit to: Lennart Kloppenburg

session_start(); // This starts the session which is like a cookie, but it isn't saved on your hdd and is much more secure.

include_once('database_connection.php');

if(isset($_SESSION['loggedin']))
{ 
         header("Location: account.php");
	
} // That bit of code checks if you are logged in or not, and if you are, you can't log in again!
if(isset($_POST['submit']))
{
   $name = mysql_real_escape_string($_POST['username']); // The function mysql_real_escape_string() stops hackers!
   $pass = mysql_real_escape_string($_POST['password']); // We won't use MD5 encryption here because it is the simple tutorial, if you don't know what MD5 is, dont worry!
   $mysql = mysql_query("SELECT * FROM `dbhomework`.`users` WHERE name = '{$name}' AND password = '{$pass}'"); // This code uses MySQL to get all of the users in the database with that username and password.
    if(mysql_num_rows($mysql) < 1)
   {
     die("Password was probably incorrect!");
   } // That snippet checked to see if the number of rows the MySQL query was less than 1, so if it couldn't find a row, the password is incorrect or the user doesn't exist!
   $_SESSION['loggedin'] = "YES"; // Set it so the user is logged in!
   $_SESSION['name'] = $name; // Make it so the username can be called by $_SESSION['name']
   header("Location: account.php");
}
	 // That bit of code logs you in! The "$_POST['submit']" bit is the submission of the form down below VV

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link rel="stylesheet" href="quiz.css" type="text/css" />
</head>
<body>
<div id="container">

	<h1>Login</h1>

<form action= 'index.php' type='login.php' method='POST'>
<p>Username: <br>
<input type='text' name='username'><br></p>
<p>Password: <br>
<input type='password' name='password'><br></p>
<p><input type='submit' name='submit' value='Log in'></p>
</form>
<p>You can <a href="registration.php">register</a> here.</p>
</div>
</body>
</html>