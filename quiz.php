<?php

session_start();

if(!isset($_SESSION['loggedin']))
{
    die("To access this page, you need to <a href='index.php'>LOGIN</a>"); // Make sure they are logged in!
} // What the !isset() code does, is check to see if the variable $_SESSION['loggedin'] is there, and if it isn't it kills the script telling the user to log in!


if(!$_SESSION){

do_it();
}
include ('database_connection.php');
include ('functions.php');

//I got this code from a forum. It was said that session.gc_maxlifetime and session.cookie_lifetime were not reliable.
//Source:http://stackoverflow.com/questions/520237/how-do-i-expire-a-php-session-after-30-minutes
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > (60*60*24*7))) {
    // last request was more than 7 days ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
} else if (time() - $_SESSION['CREATED'] > (1800)) {
    // session started more than 30 minutes ago
    session_regenerate_id(true);    // change session ID for the current session an invalidate old session ID
    $_SESSION['CREATED'] = time();  // update creation time
}

//That function gives all $_POST values to the $_SESSION values with the same key.
if(isset($_POST)){
stripPost($_POST);
}
if(!isset($_SESSION["correctcount"])){
$_SESSION["correctcount"] = 1;
}
if(isset($_SESSION["answer"])&&isset($_SESSION["rightanswer"])){
if(checkAnswer()){
$_SESSION["correctcount"]++;
}
}
if(!isset($_SESSION["currentquestion"])){
$_SESSION["currentquestion"] = 1;
}
//Checks if questions have been set already or not, and creates them if not.
function do_it(){
if(!isset($_SESSION["questions"])){
$questionquery = "select q_number, q_text from dbhomework.question";
$getquestions = mysql_query($questionquery) or die(mysql_error());
$questions = array();
while($row = mysql_fetch_assoc($getquestions)){ #If I use "array" instead of "assoc", it will print the results twice. I don't know why.
foreach($row as $result){
$questions[intval($row["q_number"])] = $row["q_text"];
}
}
$_SESSION["questions"] = $questions;

}

else if($_SESSION["currentquestion"]>count($_SESSION["questions"])){
$Score = $_SESSION["correctcount"];
echo '<head>';
    echo ' <link rel="stylesheet" href="quiz.css" type="text/css">';
echo '</head>';
echo "<div id=\"container\">";
$query_update = "update dbhomework.users set best_score=$Score where name=\"{$_SESSION["name"]}\"";
echo "<p>You have finished the quiz.</p>";
if($_SESSION["bestscore"]<0){
echo "<p>This was your first time. Your score was {$Score}, and it has been submitted.</p>";

mysql_query($query_update) or die (mysql_error());
}
else if($Score>$_SESSION["bestscore"]){
echo "<p>You did better than your best!<br/>Your best was: {$_SESSION["bestscore"]} points,<br/>whereas this time, you scored: $Score points. Congratulations!</p>";
mysql_query($query_update) or die (mysql_error()) ;
}
else if($_SESSION["bestscore"]==count($_SESSION["questions"])&& $Score == count($_SESSION["questions"])){
echo "<p>You already scored the maximum amount of points, which is $Score points.</p>";

}
else{
echo "<p>What's going on? You have done this test better before. <br/>Best Score: {$_SESSION["bestscore"]} points,<br/>Current Score: $Score points. Better luck next time!</p>";
}

echo "<br/>Click <a href=\"account.php\">here</a> to return to the portal";
echo "</div>";

unset($_SESSION["correctanswer"]);
unset($_SESSION["answer"]);
unset($Score);
unset($bestscore);
$_SESSION["currentquestion"] = 1;
$_SESSION["correctcount"] = 0;
}
else{
//Creates the answers to the current question
getAnswers();
getNewRightAnswer();
//This will print the form. 
printForm();

}
}
do_it();
?>